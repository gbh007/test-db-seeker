package tests

import "app/internal/utils"

func SeekWrite() error {
	simpleData := []byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 0}
	writeData := []byte{11, 12, 13, 14}
	alterData := []byte{1, 2, 3, 11, 12, 13, 14, 8, 9, 0}

	err := utils.Create(Filename, simpleData)
	if err != nil {
		return err
	}

	err = utils.CompareFile(Filename, simpleData)
	if err != nil {
		return err
	}

	const (
		offset = 3
	)

	err = utils.Write(Filename, writeData, offset)
	if err != nil {
		return err
	}

	err = utils.CompareFile(Filename, alterData)
	if err != nil {
		return err
	}

	err = utils.Delete(Filename)
	if err != nil {
		return err
	}

	return nil
}
