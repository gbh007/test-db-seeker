package tests

import "app/internal/utils"

func Basic() error {
	simpleData := []byte{1, 2, 3, 4, 5}

	err := utils.Create(Filename, simpleData)
	if err != nil {
		return err
	}

	err = utils.CompareFile(Filename, simpleData)
	if err != nil {
		return err
	}

	err = utils.Delete(Filename)
	if err != nil {
		return err
	}

	return nil
}
