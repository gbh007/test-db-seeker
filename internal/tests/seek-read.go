package tests

import "app/internal/utils"

func SeekRead() error {
	simpleData := []byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 0}

	err := utils.Create(Filename, simpleData)
	if err != nil {
		return err
	}

	err = utils.CompareFile(Filename, simpleData)
	if err != nil {
		return err
	}

	const (
		offset = 3
		limit  = 5
	)

	readedData, err := utils.Read(Filename, offset, limit)
	if err != nil {
		return err
	}

	err = utils.CompareData(readedData, simpleData[offset:offset+limit])
	if err != nil {
		return err
	}

	err = utils.Delete(Filename)
	if err != nil {
		return err
	}

	return nil
}
