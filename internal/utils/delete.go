package utils

import (
	"os"
)

func Delete(filename string) error {
	err := os.Remove(filename)
	if err != nil {
		return err
	}

	return nil
}
