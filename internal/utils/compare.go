package utils

import (
	"errors"
	"io"
	"os"
)

var NotEqualErr = errors.New("not equal")

func CompareFile(filename string, dataToCompare []byte) error {
	file, err := os.Open(filename)
	if err != nil {
		return err
	}

	defer file.Close()

	data, err := io.ReadAll(file)
	if err != nil {
		return err
	}

	err = CompareData(data, dataToCompare)
	if err != nil {
		return err
	}

	return nil
}

func CompareData(a, b []byte) error {
	if len(a) != len(b) {
		return NotEqualErr
	}

	for index := range a {
		if a[index] != b[index] {
			return NotEqualErr
		}
	}

	return nil
}
