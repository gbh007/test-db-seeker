package utils

import (
	"io"
	"os"
)

func Read(filename string, offset, limit int64) ([]byte, error) {
	file, err := os.OpenFile(filename, os.O_CREATE|os.O_RDWR, os.ModePerm)
	if err != nil {
		return nil, err
	}

	defer file.Close()

	_, err = file.Seek(offset, io.SeekStart)
	if err != nil {
		return nil, err
	}

	data := make([]byte, limit)

	l, err := file.Read(data)
	if err != nil {
		return nil, err
	}

	return data[:l], nil
}
