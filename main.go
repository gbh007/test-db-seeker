package main

import (
	"app/internal/tests"
	"fmt"
)

type toTest struct {
	name string
	f    func() error
}

func main() {
	to := []toTest{
		{
			name: "Basic",
			f:    tests.Basic,
		},
		{
			name: "SeekRead",
			f:    tests.SeekRead,
		},
		{
			name: "SeekWrite",
			f:    tests.SeekWrite,
		},
	}

	l := len(to)

	for index, v := range to {
		fmt.Printf("%2d/%2d %20s:", index+1, l, v.name)
		err := v.f()
		if err != nil {
			fmt.Printf(" FAIL > %v\n", err)
		} else {
			fmt.Printf(" OK\n")
		}
	}
}
