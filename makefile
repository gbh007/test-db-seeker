create_build_dir:
	mkdir -p ./_build

build: create_build_dir
	CGO_ENABLED=0 GOOS=linux GOARCH=arm64 go build  -o ./_build/tds-linux-arm64
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o ./_build/tds-linux-amd64
	CGO_ENABLED=0 GOOS=windows GOARCH=amd64 go build  --o ./_build/tds-windows-amd64.exe
	CGO_ENABLED=0 GOOS=darwin GOARCH=arm64 go build  -o ./_build/tds-darwin-arm64
	CGO_ENABLED=0 GOOS=darwin GOARCH=amd64 go build -o ./_build/tds-darwin-amd64

run: create_build_dir
	go build  -o ./_build/bin
	./_build/bin

